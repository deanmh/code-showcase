# Dean Mehinovic, 2018. All rights reserved.
# Tennis match Monte Carlo simulator

import random
import sys

class Match:
    def __init__(self, best_of_sets = 3):
        # Specify the number of sets in this match.
        self.best_of_sets = best_of_sets      

    def play(self, player_1, player_2):
        """ Play a virtual tennis match. """
        s = Set()
        set_scores = [0, 0]
                
        while True:
            # Set the first player to serve in a set depending on the number of 
            # games played in the last set. 
            if s.games_tally % 2 == 0:
                won_player = s.play(player_1.serve_winpct, 
                                    player_2.serve_winpct)
                set_scores[won_player] += 1    
            else:
                won_player = s.play(player_2.serve_winpct, 
                                    player_1.serve_winpct)
                set_scores[1 - won_player] += 1
            
            # For debugging purposes:
            # print("set scores: ", set_scores)

            # If the match is over, return the winning player's number. 
            if (self.best_of_sets // 2 + 1) in set_scores:
                return won_player 

class Set:
    def __init__(self):
        self.games_tally = 0

    def play(self, player1_serve_winpct, player2_serve_winpct):
        """ Play a virtual set of tennis. """
        scores = [0, 0]
        serve_winpcts = [player1_serve_winpct, player2_serve_winpct]
        serving_player = 0
        self.games_tally = 0

        while True:
            # Play a game, record the results, and change the serving player for
            # the next game.
            game = Game(serve_winpcts[serving_player])
            won_player = serving_player if game.play() == 0 \
                                        else 1 - serving_player
            lost_player = 1 - won_player
            scores[won_player] += 1
            self.games_tally += 1
            serving_player = 1 - serving_player
            
            # For debugging purposes: 
            # print('Games score:', scores[0], scores[1])

            # Return who won if won by two games, otherwise play a tiebreak. 
            if scores[won_player] > 5: 
                if (scores[won_player] - scores[lost_player]) > 1:
                    return won_player
                elif scores[won_player] == scores[lost_player]:
                    self.games_tally += 1
                    game = Game(serve_winpcts[serving_player])
                    return game.play_tiebreak()            

class Player:
    won_matches = 0

    def __init__(self, serve_winpct, return_winpct = 0, name = "Player"):
        self.serve_winpct = serve_winpct
        self.return_winpct = return_winpct
        self.name = name

class Game:
    def __init__(self, server_1_winpct, server_2_winpct = 0):
        self.scores = [0, 0]
        self.server_1_winpct = server_1_winpct
        self.server_2_winpct = server_2_winpct
            
    def play_point(self, server_winpct):
        """ Play a single point and return the winning player's number. """
        return 0 if (random.uniform(0, 1) < server_winpct) else 1

    def denormalize_score(self, scores):
        """ Convert internal score representation into proper tennis score. """
        scores_space = [0, 15, 30, 40]
        scores_converted = [" ", " "]

        if scores[0] > 3 or scores[1] > 3:
            if scores[0] == scores[1]:
                return ["40", "40"]
            elif abs(scores[0] - scores[1]) > 1:
                return ["", ""]
            advantage = 0 if scores[0] > scores[1] else 1
            scores_converted[advantage] = "AD"
           
        else:
            scores_converted = [ scores_space[i] for i in scores ]

        return scores_converted

    def play(self):
        """ Play a single game. """
        tally = []
                
        while True:
            # Play a single point.
            won_player = self.play_point(self.server_1_winpct)
            lost_player = 1 - won_player
            self.scores[won_player] += 1
            
            # print(self.scores)
            
            # If game is over, return the number of the winning player;
            # otherwise, update scores and play another point.
            if self.scores[won_player] > 3 and \
              (self.scores[won_player] - self.scores[lost_player]) > 1:
                # winner = "Server" if won_player == 0 else "Returner"
                # print(''.join(tally), winner + " won.")
                return won_player
            else:
                denormalized_scores = self.denormalize_score(self.scores)
                tally.append('{}-{}, '.format(denormalized_scores[0],
                                              denormalized_scores[1]))

    def play_tiebreak(self):
        # Set point win percentages on serve for the players
        server_winpcts = [self.server_1_winpct, self.server_2_winpct]
        current_server = 0
        self.scores = [0, 0]
        tally = []
        
        while True:
            # Play a single tiebreak point and update player scores. 
            won_player = self.play_point(server_winpcts[current_server])
            lost_player = 1 - won_player
            self.scores[won_player] += 1

            tally.append('{} {}, '.format(self.scores[0], self.scores[1]))
            
            # Change serving player after every odd point.
            if sum(self.scores) % 2 == 1:
                current_server = 1 - current_server
                        
            # If tiebreak is finished, return the winning player's number.
            if self.scores[won_player] > 6 and \
              (self.scores[won_player] - self.scores[lost_player]) > 1:
                # Display points tally for debugging purposes: 
                # print('played tiebreak:')
                # print(''.join(tally))
                return won_player    

# Instantiate our players and set the match conditions.
players = [Player(0.71, 0.37, 'Nadal'), Player(0.7, 39, 'Djokovic')]
tally = []
m = Match(3)
coinflip = 1
num_matches = 2000

# Play a sample of 2,000 tennis matches.
for i in range(num_matches):
    # Alternate starting server for each match and record results of sim.
    coinflip = 1 - coinflip               # deprecated: random.randint(0, 1)
    won_player = m.play(players[coinflip], players[1-coinflip])
    if coinflip == 0:
        tally.append(won_player)    
    else:
        tally.append(1 - won_player)

print(num_matches, 'matches played.')    
for i in (0, 1):
    print('{} won {} matches.'.format(players[i].name, tally.count(i)))
