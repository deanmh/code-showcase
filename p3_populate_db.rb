# Dean Mehinovic, 2009.
# Update a MySQL database with medical insurance claims data from csv file.

require 'rubygems'
require 'mysql'
require 'fastercsv'
require 'ruport'

# Generate an SQL query given values and keys hash tables, a table name and 
# a query mode (:insert, :update or :delete).

def gen_sql(cols_vals_hash, keys_vals_hash, table_name, mode)
  if mode == :insert
    proper_vals = cols_vals_hash.values.map{|k| "'" + \
                                              k.to_s.gsub(/\'/){"\\'"} + "'"}
    proper_cols = cols_vals_hash.keys.map{|k| "`" + k.to_s + "`"}
    return "INSERT INTO " + table_name + " ("+proper_cols.join(",")+\
           ") VALUES ("+proper_vals.join(",")+");"
  
  elsif mode == :update
    vals_pairs = split_pairs(cols_vals_hash, :values)
    keys_pairs = split_pairs(keys_vals_hash, :keys)
    return "UPDATE "+table_name+" SET "+vals_pairs+" WHERE "+keys_pairs+";"
  
  elsif mode == :delete
    keys_pairs = split_pairs(keys_vals_hash, :keys)
    return "DELETE FROM "+table_name+" WHERE "+keys_pairs+";"
  end
end

# Read a csv file into a Ruport data table object.

def read_csv(file_name)
  t = Ruport::Data::Table.new
  
  FasterCSV.foreach(file_name, "r") do |row|
      if row[4].to_s =~ /\d/
        t << row
      else
        t = Ruport::Data::Table.new(:column_names => row)
      end
  end
 
  return t
end

# Construct a segment of SQL from a hash table. The former ends up containing
# either value assignments or key value specifications, depending on the mode 
# selected.

def split_pairs(pairs_hash, mode = :values)
  join_with = ","           

  # If the mode is :values, join value pairs with "," in the SQL. Otherwise, 
  # join the key pairs in the SQL with " AND ".
  if mode == :keys
    join_with = " AND "
  end

  return pairs_hash.to_a.map {|pair| "`" + pair[0].to_s.gsub(/\'/){"\\'"} + \
            "`" + "='" + pair[1].to_s.gsub(/\'/){"\\'"} + "'"}.join(join_with)
end

# Update the database with claims from the csv input file. 
dbh = Mysql.real_connect("localhost", "root", "", "claim_production")
col_names = []
t = read_csv("k-merge2.csv")

t.each do |row|
  values_hash = {}
  keys_hash = {}
  keys = ["PRIMARY/SECONDARY", "SENT", "PATIENT NAME", "SERVICE", "BIRTHDAY", \
          "TOTAL"] 

  keys.each {|key| keys_hash.merge!({key.to_s.gsub(/\s/,"_") => row[key].to_s})}
  row.attributes.each {|colname| values_hash.merge!( \
                       {colname.to_s.gsub(/\s/,"_") => row[colname].to_s})}
  response = dbh.query("SELECT * FROM claims WHERE " + split_pairs(keys_hash, \
                       :keys) + ";")

  # If database records match a given csv entry, increment the matching record's 
  # tally in the 'claims' table. Otherwise, insert the new claim into the table.
  if response.fetch_row
    dbh.query("UPDATE claims SET NUM_OCCUR = NUM_OCCUR + 1 WHERE " + \
              split_pairs(keys_hash, :keys) + ";")
  else
    dbh.query(gen_sql(values_hash, keys_hash, "claims", :insert))  
  end
end

# Per the program specifications, if an existing database record does not match 
# any of the claims in the input file, make that database record invisible.
db_all_claims = dbh.query("SELECT * FROM claims;")
row = db_all_claims.fetch_hash
primkeys = ["PRIMARY/SECONDARY", "SENT", "PATIENT NAME", "SERVICE", \
            "BIRTHDAY", "TOTAL"]
begin
  primkeys_hash = {}
  primkeys.each {|key| primkeys_hash.merge!({key => \
                       row[key.to_s.gsub(/\s/,"_")].to_s})}

  if t.rows_with(primkeys_hash.keys) {|a,b,c,d,e,f| [a,b,c,d,e,f] == \
                                      primkeys_hash.values}  == [] 
    primkeys_hash = {}
    primkeys.each {|key| primkeys_hash.merge!({key.to_s.gsub(/\s/,"_") => \
                         row[key.to_s.gsub(/\s/,"_")].to_s})}
    dbh.query(gen_sql({"VISIBLE" => "no"}, primkeys_hash, "claims", :update))
  end
end while row = db_all_claims.fetch_hash
 