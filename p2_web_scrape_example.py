# Dean Mehinovic, 2018. All rights reserved.
# Choose a heads up tennis players matchup and scrape its webpage from the 
# Association of Tennis Professionals website. Use XPath to parse the json match
# data from the webpage. Display the tennis match data, count the total number 
# of sets won by each player, and display the number of sets won by each player. 

import sys
from lxml import html
import requests
import json
import pandas as pd
import re
import pyperclip

# To avoid hammering the server during development, get the webpage from file 
# rather than from the web. 
# with open('h2h_fed_djo.htm', 'r') as htmlsource:
#     page = htmlsource.read()

url = ("https://www.atpworldtour.com/en/players/fedex-head-2-head/roger-"
       "federer-vs-kei-nishikori/f324/n552")

# Alternatively, get the url from the system clipboard using pyperclip.paste().
# url = pyperclip.paste() 

page = requests.get(url).content
doctree = html.fromstring(page)

json_raw = doctree.xpath(
                        "(//script[@id='playerDataModalDefault']/text())[1]")[0]
jdata = json.loads(str(json_raw))

sets_won = {}
for player in ['playerLeft', 'playerRight']: 
    player_name = jdata[player]['firstName'] + " " + jdata[player]['lastName'] 
    sets_won[jdata[player]['id']] = { 'name' : player_name, 'won' : 0 }

for tourney in jdata['Tournaments']:
    match_results = tourney['MatchResults'][0]
    # disregard this match if it was not completed
    if match_results['Reason'] is not None:
        continue       

    field_names = [['EventYear'], ['TournamentName'], ['TournamentLocation',    
                    'EventLocation'], ['TournamentLocation', 'EventCity'], 
                   ['Surface'], ['InOutdoorDisplay'], ['MatchResults', 0, 
                    'Round', 'ShortName'], ['MatchResults', 0, 'Winner']] 
    # Load data for a given match and display it. 
    match = []
    for field_name in field_names:
        field_value = tourney
        for key in field_name:
            field_value = field_value[key]
        match.append(field_value)
    print(match)

    # Tally up the number of sets won by each player in a given match.        
    for a_set in zip(match_results['PlayerTeam']['Sets'][1: ], 
                     match_results['OpponentTeam']['Sets'][1: ]):
        winner = 'PlayerTeam' if a_set[0]['SetScore'] > a_set[1]['SetScore'] \
                              else 'OpponentTeam'
        sets_won[match_results[winner]['PlayerId']]['won'] += 1

print('--------------------------------------------------------------')
for player_num in list(sets_won):
    print("Player {} won {} sets.".format(sets_won[player_num]['name'],
                                          sets_won[player_num]['won']))  
 
