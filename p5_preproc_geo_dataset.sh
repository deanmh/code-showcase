#!/bin/bash

### Dean Mehinovic for Yale School of Forestry and Environmental         ###
### Studies, 2020.                                                       ###
### This script accessess a water table dataset previously downloaded    ###
### with another script, then preprocesses multiple NetCDF array images  ###
### of worldwide water table levels. It then merges these images into a  ###
### single GeoTIFF file, enabling processing further down the pipeline.  ###

### first make sure the present working directory is where the script is ###
### located                                                              ###

cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

### makse sure the corners of all the NetCDF images align and convert    ### 
### them into GeoTIFF images                                             ###
for nc_filepath in ../datasets/gpgtd/*.nc ; do

  nc_filepath_trimmed=$(echo "$nc_filepath" | grep -oP ".+(?=\.nc)" )
  tiff_name="$nc_filepath_trimmed".tif

  gdal_translate -co ZLEVEL=9 -of GTiff NETCDF:\"$nc_filepath\":WTD $tiff_name

  ### get geo. coordinates of upper left and lower right raster corners  ###
  
  corners=$(getCorners4Gtranslate $tiff_name)
  rnd_ul_x=$(printf "%.f" $(echo $corners | cut -d ' ' -f 1))
  rnd_ul_y=$(printf "%.f" $(echo $corners | cut -d ' ' -f 2))
  rnd_lr_x=$(printf "%.f" $(echo $corners | cut -d ' ' -f 3))
  rnd_lr_y=$(printf "%.f" $(echo $corners | cut -d ' ' -f 4))

  ### clip the raster with rounded coords. and put it into a new GTiff   ###
  
  gdal_edit.py -a_nodata -9999 -a_ullr $rnd_ul_x $rnd_ul_y \
	                 $rnd_lr_x  $rnd_lr_y $tiff_name
done

### Piece together a virtual format file from all the different maps,    ###
### then make a GTiff from it.                                           ### 

gdalbuildvrt -srcnodata -9999 -vrtnodata -9999  globe.vrt *.tif  
gdal_translate -a_srs EPSG:4326 -a_nodata -9999 -co COMPRESS=DEFLATE \
	                            -co ZLEVEL=9 globe.vrt globe.tif

pksetmask -co COMPRESS=DEFLATE -co ZLEVEL=9 -m $nc_file -msknodata -9999 \
                     -data 0 -of GTiff -i globe.tif -o  globe_final.tif

